var data = [
    "Muhammad Iqbal Mubarok",
    "Ruby Purwati",
    "Faqih Muuhammad",
];


function tampil() {
    var tabel = document.getElementById("tabel");
    tabel.innerHTML = "<tr><th></th><th></th><th></th></tr>";
    for (let i = 0; i < data.length; i++)
    {
        var btnEdit = "<button class='btn-edit' href='#' onclick='edit(" + i + ")'>Edit</button>";
        var btnDelete = "<button class='btn-delete' href='#' onclick='delete(" + i + ")'>Delete</button>";
        j = i + 1;
        tabel.innerHTML += "<tr><td>" + j + "</td><td>" + data[i] + "</td><td>" + btnEdit + " " + btnDelete + "</tr>";
    }
}

function tambah()
{
    var input = document.querySelector("input[name=nama]");
    data.push(input.value);
    tampil();
    input.value = "";
}

function edit(id)
{
    var baru = prompt("Edit", data[id]);
    data[id] = baru;
    tampil();
}

function hapus(id)
{
    data.pop(id);
    tampil();
}

tampil();
