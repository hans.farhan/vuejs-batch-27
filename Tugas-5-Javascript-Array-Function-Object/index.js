//soal no 1
console.log("------------------------------------------")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

daftarHewan.sort()
daftarHewan.forEach(function(daftarHewan){
    console.log(daftarHewan) //jawaban soal no 1
})

console.log("------------------------------------------")
//soal no 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming"}
function introduce ()
{
console.log('Nama saya '+data.name+','+' umur saya '+data.age+','+' alamat saya di jalan '+data.address+','+' dan saya punya hobby yaitu '+data.hobby);
}
introduce(); //jawaban soal no 3

console.log("------------------------------------------")
//soal no 3
function hitung_huruf_vokal(params) {  //jawaban soal no 3
    var vocalCount = 0 
    for (const i in params) {
        var kata = params[i].toLowerCase()
        if (kata == "a" || kata == "i" || kata == "u" || kata == "e" || kata == "o") {
            vocalCount += 1
        }
    }
    return vocalCount
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2)

console.log("------------------------------------------")
//soal no 4
function hitung(params) {
    return params * 2 - 2
}

console.log(hitung(0)) //jawaban soal no 4
console.log(hitung(1)) 
console.log(hitung(2)) 
console.log(hitung(3)) 
console.log(hitung(5)) 

