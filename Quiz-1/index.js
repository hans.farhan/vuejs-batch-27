function next_date(tanggal, bulan, tahun)
  {
 if (tanggal > 31 || bulan > 12)
  {
   return "Error"
  }
    
 var namaBulan = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"]
  
 var next_tanggal = tanggal + 1
 var next_bulan = namaBulan[bulan - 1]
 var next_tahun = tahun
 var hasil = `${next_tanggal} ${next_bulan} ${next_tahun}`
  
 if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12)
 {
  if (tanggal == 31)
    {
     next_tanggal = 1
      next_bulan = bulan == 12 ? namaBulan[0] : namaBulan[bulan]
      next_tahun = bulan == 12 ? tahun + 1 : tahun
      hasil = `${next_tanggal} ${next_bulan} ${next_tahun}`
    }
  }
  else if (bulan == 2 && tahun % 4 == 0)
  {
    if (tanggal < 29)
    {
      next_tanggal = tanggal + 1
    } 
      else 
    {
      next_tanggal = 1
      next_bulan = namaBulan[bulan]
    }
  }
  if (next_tanggal > 28 && bulan == 2 && tahun % 4 != 0)
  {
   return "Error"
  }
  return hasil
}
console.log(next_date(31, 3, 2019))
